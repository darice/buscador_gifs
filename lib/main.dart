import 'package:flutter/material.dart';
import 'package:buscador_gifs/pages/home_page.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:share/share.dart';
import 'functions.dart';
import 'package:buscador_gifs/pages/gif_page.dart';
import 'package:buscador_gifs/pages/favoritos_page.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/home',
    routes: <String, WidgetBuilder>{
      '/home': (BuildContext context) => new HomePage(),
      '/favoritos': (BuildContext context) => new FavoritosPage(),
      '/gif': (BuildContext context) => new GifPage(),
    },
    theme: ThemeData(
      hintColor: colorMain,
      primaryColor: colorMain,
    ),
    debugShowCheckedModeBanner: false,
  ));
}

Color colorMain = Colors.yellow;
final pesquisaControle = TextEditingController();
String pesquisa = '';
int pagina = 0;
List favoritos = [];
Map pegarOGif;
bool paginaAnteriorFavoritos;

Widget mostrarGif(BuildContext context, x, index) {
  return GestureDetector(
    child: FadeInImage.memoryNetwork(
      placeholder: kTransparentImage,
      image: x['images']['fixed_height_small']['url'],
      // height: 300,
      fit: BoxFit.cover,
    ),
    onTap: () {
      pegarOGif = x;
      mudarPagina(context: context, pagina: GifPage());
    },
    onLongPress: () {
      Share.share(
        x['images']['fixed_height_small']['url'],
      );
    },
  );
}
