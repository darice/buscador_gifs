import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:buscador_gifs/main.dart';

Future<File> obterArquivo() async {
  final diretorio = await getApplicationDocumentsDirectory();
  return File("${diretorio.path}/dados.json");
}

Future<File> salvarDados() async {
  String dados = json.encode(favoritos);
  final arquivo = await obterArquivo();
  return arquivo.writeAsString(dados);
}

Future<String> lerDados() async {
  try {
    final arquivo = await obterArquivo();
    return arquivo.readAsString();
  } catch (e) {
    return null;
  }
}
