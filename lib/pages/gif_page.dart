import 'package:buscador_gifs/functions.dart';
import 'package:buscador_gifs/pages/favoritos_page.dart';
import 'package:flutter/material.dart';
import 'package:buscador_gifs/main.dart';
import 'package:share/share.dart';
import 'package:buscador_gifs/arquivo.dart';

class GifPage extends StatefulWidget {
  @override
  _GifPageState createState() => _GifPageState();
}

class _GifPageState extends State<GifPage> {
  void addFavorito() {
    setState(() {
      Map<String, dynamic> novoFavorito = Map();
      novoFavorito['gif'] = pegarOGif;
      favoritos.add(novoFavorito);
      salvarDados();
    });
  }

  void removeFavorito(x) {
    int i;
    for (i = 0; i < favoritos.length; i++) {
      if (favoritos[i]['gif']['id'] == x) {
        setState(() {
          favoritos.removeAt(i);
          salvarDados();
        });
        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        iconTheme: IconThemeData(color: colorMain),
        title: Text(
          pegarOGif['title'],
          style: TextStyle(color: colorMain),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            if (paginaAnteriorFavoritos) {
              mudarPagina(context: context, pagina: FavoritosPage());
            } else {
              Navigator.of(context).pop();
            }
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: iconeFavorito(),
            onPressed: () {
              pertenceFavoritos(pegarOGif['id'])
                  ? removeFavorito(pegarOGif['id'])
                  : addFavorito();
            },
          ),
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              Share.share(pegarOGif['images']['original']['url']);
            },
          ),
        ],
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Image.network(pegarOGif['images']['original']['url']),
      ),
    );
  }

  Widget iconeFavorito() {
    return Icon(pertenceFavoritos(pegarOGif['id'])
        ? Icons.favorite
        : Icons.favorite_border);
  }
} //fim
