import 'package:buscador_gifs/pages/favoritos_page.dart';
import 'package:flutter/material.dart';
import 'package:buscador_gifs/main.dart';
import 'package:buscador_gifs/functions.dart';
import '../arquivo.dart';
import 'favoritos_page.dart';
import 'dart:convert';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    paginaAnteriorFavoritos = false;
    // pegarGifs().then((map) => print(map));
    lerDados().then((dados) {
      setState(() {
        favoritos = json.decode(dados);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.network(
          "https://br.web.img2.acsta.net/newsv7/15/07/23/20/23/332921.png",
        ),
        centerTitle: false,
        backgroundColor: colorMain,
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.favorite,
              ),
              alignment: Alignment.centerLeft,
              onPressed: () {
                mudarPagina(context: context, pagina: FavoritosPage());
              }),
        ],
      ),
      backgroundColor: Colors.black,
      body: body(),
    );
  }

  Widget body() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(10),
          child: textField(),
        ),
        Expanded(
          child: FutureBuilder(
            future: pegarGifs(),
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return carregando();
                default:
                  if (snapshot.hasError)
                    return erro();
                  else
                    return tabelaGifs(context, snapshot);
              }
            },
          ),
        )
      ],
    );
  }

  Widget textField() {
    return TextField(
      onSubmitted: (texto) {
        setState(() {
          pesquisa = texto;
          pagina = 0;
        });
      },
      cursorColor: colorMain,
      style: TextStyle(
        color: colorMain,
        fontSize: 18.0,
      ),
      textAlign: TextAlign.center,
      decoration: InputDecoration(
        labelText: "Pesquise aqui!",
        labelStyle: TextStyle(color: colorMain),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: colorMain)), //borda sem click
        border: OutlineInputBorder(), //borda quando click
      ),
    );
  }

  Widget carregando() {
    return Container(
      width: 200,
      height: 200,
      alignment: Alignment.center,
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation(colorMain),
      ),
    );
  }

  Widget erro() {
    return Container(
      child: Center(
        child: Icon(
          Icons.signal_wifi_off,
          size: 70,
          color: colorMain,
        ),
      ),
    );
  }

  Widget tabelaGifs(BuildContext context, AsyncSnapshot snapshot) {
    return GridView.builder(
      padding: EdgeInsets.all(10),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
      itemCount: pegarCount(snapshot.data['data']),
      itemBuilder: (context, index) {
        if (pesquisa == '' || index < snapshot.data['data'].length) {
          return mostrarGif(context, snapshot.data['data'][index], index);
        } else {
          return carregarMais();
        }
      },
    );
  }

  Widget carregarMais() {
    return GestureDetector(
      onTap: () {
        setState(() {
          pagina += 13;
        });
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.add,
              size: 60,
              color: colorMain,
            ),
            Text(
              "Carregar mais...",
              style: TextStyle(color: colorMain, fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }
} //fim da HomePageState
