import 'package:flutter/material.dart';
import 'package:buscador_gifs/main.dart';
import 'package:buscador_gifs/arquivo.dart';
import 'dart:convert';

class FavoritosPage extends StatefulWidget {
  @override
  _FavoritosPageState createState() => _FavoritosPageState();
}

class _FavoritosPageState extends State<FavoritosPage> {
  @override
  void initState() {
    super.initState();
    paginaAnteriorFavoritos = true;
    lerDados().then((dados) {
      setState(() {
        favoritos = json.decode(dados);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Favoritos"),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            paginaAnteriorFavoritos = false;
            Navigator.popUntil(context, ModalRoute.withName('/home'));
          },
        ),
      ),
      body: body(),
      backgroundColor: Colors.black,
    );
  }

  Widget body() {
    if (favoritos.length == 0) {
      return Center(
        child: Text(
          "Você não tem nenhum favorito",
          style: TextStyle(
            color: colorMain,
          ),
        ),
      );
    }
    return GridView.builder(
        padding: EdgeInsets.all(10),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, mainAxisSpacing: 10, crossAxisSpacing: 10),
        itemCount: favoritos.length,
        itemBuilder: (context, index) {
          return mostrarGif(context, favoritos[index]['gif'], index);
        });
  }
} //fim
